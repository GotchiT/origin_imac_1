let i = 100;
$(document).ready(function modal(){
  $(".single-img").click(function(){
    var $src = $(".single-img-content").attr("src");

    $(".show").fadeIn();
    $(".img-show img").attr("src", $src);
  })
});

function setIntervalClosePrel(){
  if (i > 0) {
    let intv = setInterval(closePrel, 10);
  }
  return false;
}

function closePrel(){
  if (i > 0) {
    let j = 100-i;
    $('.preloader').css("opacity", i+"%");
    $('.preloader').css("width", i+"vw");
    $('.preloader').css("height", i+"vh");
    $('#navBar').css("opacity", j+"%");
    i--;
  }
  if (i == 0) {
    $('.preloader').addClass('none');
    $('.body').css("overflow-y", "auto");
  }
}


$(document).ready(function myAjax(nom){
  $.ajax({
    url:"https://www.instagram.com/chocolat_origin?__a=1",
    type:'get',
    success:function(response){
      //console.log(response);
      pp = response.graphql.user.profile_pic_url;
      header_right = '<a href="https://www.instagram.com/chocolat_origin" target="blank"><img src="' + pp + '" alt="Photo de profil"><h5>Instagram</h5></a>';
      posts = response.graphql.user.edge_owner_to_timeline_media.edges;
      posts_html = '';

      for(var i=0;i<posts.length;i++){
        url = posts[i].node.display_url;
        caption = posts[i].node.accessibility_caption;
        if (i == 0) {
          posts_html += '<div class="single-img"><i class="solde">-99%</i><img class="single-img-content imgToClick" onclick="modal(this);"" src="'+ url +'" alt="'+caption+'"></div>';
        } else if (i == 3 || i == 6) {
          posts_html += '<div class="single-img"><i class="solde">-15%</i><img class="single-img-content imgToClick" onclick="modal(this);" src="'+ url +'" alt="'+caption+'"></div>';
        } else {
          posts_html += '<div class="single-img"><img class="single-img-content imgToClick" onclick="modal(this);"" src="'+ url +'" alt="'+caption+'"></div>';
        }
      }
      $('#header_right').html(header_right);

      $("#gallery").html(posts_html);
    },
    error:function(){
      alert('This name doesn\'t exist !');
    }
  });
});


function modal(e) {
  // Get the modal
  let modal = document.getElementById("myModal");
  // Get the image and insert it inside the modal - use its "alt" text as a caption
  let img = e;
  let modalImg = document.getElementById("img01");
  let captionText = document.getElementById("caption");
  img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
    //console.log(captionText);
    document.getElementById('navBar').style.display = "none";
    if (window.screen.width <= 1044) {
      document.querySelector('.menu-icon').style.display = "none";
    }
  }
}

function closeModal(){
  let modal = document.getElementById("myModal");
  // When the user clicks on <span> (x), or on the document, close the modal
  if (window.screen.width <= 1044) {
      document.getElementById('navBar').style.display = "inline-block";
      document.querySelector('.menu-icon').style.display = "inline-block";
      modal.style.display = "none";
      return 0;
  }
  document.getElementById('navBar').style.display = "flex";
  modal.style.display = "none";
}
